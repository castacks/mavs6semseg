# mavs6semseg

Daniel Maturana (dimatura@gmail.com), 2018

ROS node to for semantic segmentation.

## License

BSD, see LICENSE.
