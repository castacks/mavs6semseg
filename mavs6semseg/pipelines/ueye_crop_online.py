"""
For online operation.
"""

from fosforo.parafina.op.cnnpreproc import CnnPreprocess
from fosforo.parafina.op.geom_tf import SimpleRescaleCrop
from fosforo.parafina.op.ros import MessageToImage, ImageToMessage
from fosforo.parafina.pipeline.line_section import LineSection
# from fosforo.parafina.op.photo_tf import ColorCorrect
# from fosforo.parafina.op.pytorch import NumpyToTorch


def build_test_ops(camera_params, publish_img_pp):
    cp = camera_params

    ops = LineSection()
    ops['msg2img'] = MessageToImage({'rgb_msg': 'rgb_image'},
                                    desired_encoding='bgr8')

    ops['croprescale'] = SimpleRescaleCrop(mapping={'rgb_image': 'rgb_image_pp'},
                                           min_size=0,  # no prescale
                                           rows=cp.pre_scale_shape[0],  # crop to square
                                           shape_ratio=(1, 1),  # preserve aspect ratio
                                           post_rows=cp.post_scale_shape[0])  # scale

    # ops['colorcorrect'] = ColorCorrect(mapping={'rgb_image_pp': 'rgb_image_pp'})

    # for visualization purposes mostly
    if publish_img_pp:
        ops['img2msg'] = ImageToMessage(mapping={'rgb_image_pp': 'rgb_image_pp_msg'},
                                        compression='none')

    # mean subtraction, data reordering, etc
    ops['cnnppx'] = CnnPreprocess(mapping={'rgb_image_pp': 'cnn_image'},
                                  to_c01=True,
                                  to_bgr=False,
                                  # to_bgr=True,
                                  dtype='float32')
    # ops['cnnppx2torch'] = NumpyToTorch({'cnn_image': 'cnn_image'},
    #                                    type_cast_map={'cnn_image': 'torch.FloatTensor'})
    return ops
