"""
For training from youtube aerial (and similar) data.
"""

from collections import OrderedDict

import cv2
from spath import Path

from fosforo.parafina.op.cnnpreproc import CnnPreprocess
from fosforo.parafina.op.geom_tf import ApplyCrop
from fosforo.parafina.op.geom_tf import ApplyImageTfs
from fosforo.parafina.op.geom_tf import ComputeImageTfs
from fosforo.parafina.op.geom_tf import GenerateRandomCrop
from fosforo.parafina.op.geom_tf import SampleGeometricJitterTf
from fosforo.parafina.op.geom_tf import SimpleRescaleCrop
from fosforo.parafina.op.img_io import CvDecodeImage
# from fosforo.parafina.op.img_io import RgbToBgr
from fosforo.parafina.op.img_io import LoadRawFile
from fosforo.parafina.op.labels import DilateBinary
from fosforo.parafina.op.img_io import PilDecodeLabelImage
from fosforo.parafina.op.labels import DilateGray
from fosforo.parafina.op.misc import UnsqueezeFirstDim
from fosforo.parafina.op.labels import LabelImageRemap
from fosforo.parafina.op.photo_tf import ApplyPhotoJitter
from fosforo.parafina.op.photo_tf import ApplyGrayPhotoJitter
from fosforo.parafina.pipeline import LineSection


#label_mapping = {0: 0,
#                 1: 1,
#                 2: 0,
#                 3: 0,
#                 4: 0,
#                 5: 0,
#                 6: 0,
#                 7: 0,
#                 8: 0,
#                 9: 0,
#                 10: 0,
#                 }

label_mapping = None

def build_train_ops(x_shape,
                    y_shape,
                    x_key='rgb_image',
                    y_key='labels_image',
                    precrop_row_range=(400, 700)):
    ops = LineSection()

    ops['gencrop'] = GenerateRandomCrop(mapping={x_key: 'rcrop'},
                                        rows_range=precrop_row_range,
                                        shape_ratio=(1, 1))

    ops['apply_rgb_crop'] = ApplyCrop({(x_key, 'rcrop'): x_key})

    ops['apply_lbl_crop'] = ApplyCrop({(y_key, 'rcrop'): y_key})

    if label_mapping:
        ops['relabel'] = LabelImageRemap(mapping={y_key: y_key},
                                         label_mapping=label_mapping)
    # ensure labels don't get lost when downsizing
    ops['dilate_lbl'] = DilateBinary({y_key: y_key}, pixels=4)

    # TODO can we do without the crop and just use zoom/translation?
    ops['samplejit'] = SampleGeometricJitterTf(mapping={'_': 'geom_jit_params'},
                                               translation_range_w_px=(-20, 20),
                                               rotation_range_deg=(-15, 15),
                                               zoom_range=(1/1.2, 1.2),
                                               do_flip=True)

    ops['comptfx'] = ComputeImageTfs(mapping={x_key: 'rgb_tf'},
                                     tf_key='geom_jit_params',
                                     pre_scale_shape=(500, 500),  # target_h,target_w
                                     crop_to_shape=(448, 448),
                                     post_scale_shape=x_shape)

    ops['comptfy'] = ComputeImageTfs(mapping={y_key: 'labels_tf'},
                                     tf_key='geom_jit_params',
                                     pre_scale_shape=(500, 500),  # target_h,target_w
                                     crop_to_shape=(448, 448),
                                     post_scale_shape=y_shape)

    ops['apptfx'] = ApplyImageTfs(mapping={(x_key, 'rgb_tf'): x_key},
                                  interp='linear',
                                  dtype='uint8')

    ops['grayphoto'] = ApplyGrayPhotoJitter(mapping={x_key: x_key})

    ops['photo'] = ApplyPhotoJitter(mapping={x_key: x_key})

    ops['apptfy'] = ApplyImageTfs(mapping={(y_key, 'labels_tf'):
                                           y_key},
                                  interp='nearest',
                                  dtype='float32')

    ops['dilate_lbl2'] = DilateGray({y_key: y_key}, pixels=2)
    ops['cnnppx'] = CnnPreprocess(mapping={x_key: x_key},
                                  to_c01=True,
                                  to_bgr=True,
                                  dtype='float32')

    ops['cnnppy'] = CnnPreprocess(mapping={y_key: y_key},
                                  mean_px=None,
                                  to_c01=True,
                                  to_bgr=False,
                                  dtype='int64')
    ops['unsqy'] = UnsqueezeFirstDim({y_key: y_key})
    return ops


def build_valid_ops(x_shape, y_shape, x_key, y_key):
    ops = LineSection()

    if label_mapping:
        ops['relabel'] = LabelImageRemap(mapping={y_key:
                                                  y_key},
                                         label_mapping=label_mapping)

    ops['dilate_lbl'] = DilateBinary({'labels_image': 'labels_image'}, pixels=4)
    ops['croprescalex'] = SimpleRescaleCrop(mapping={x_key: x_key},
                                            min_size=896,
                                            rows=896,  # crop to square
                                            shape_ratio=(1, 1),  # preserve aspect ratio
                                            post_rows=x_shape[2],  # scale
                                            interp='linear')

    ops['croprescaley'] = SimpleRescaleCrop(mapping={y_key: y_key},
                                            min_size=896,
                                            rows=896,
                                            shape_ratio=(1, 1),
                                            post_rows=y_shape[2],
                                            interp='nearest')

    ops['cnnppx'] = CnnPreprocess(mapping={x_key: x_key},
                                  to_c01=True,
                                  to_bgr=True,
                                  dtype='float32')

    ops['cnnppy'] = CnnPreprocess(mapping={y_key: y_key},
                                  mean_px=None,
                                  to_c01=True,
                                  to_bgr=False,
                                  dtype='int64')
    return ops
