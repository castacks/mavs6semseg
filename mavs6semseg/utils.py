import copy
import json
from collections import OrderedDict

import numpy as np
import rospy
import cv_bridge
import cv2

from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import Image
from semantic_segmentation_msgs.msg import LabelsTensor
from semantic_segmentation_msgs import ndma

# from fosforo.utils.distinct_colors import distinct_colors_int


bridge = cv_bridge.CvBridge()


def softmax(x):
    """ softmax across axis 0 """
    max_x = np.max(x, axis=0, keepdims=True)
    exp = np.exp(x-max_x)
    sum_exp = np.sum(exp, axis=0, keepdims=True)
    return exp/sum_exp


def init_traits_rosparam(node):
    for name in node.trait_names():
        default_param = getattr(node, name)
        ros_param = rospy.get_param('~{}'.format(name), None)
        if ros_param is None:
            rospy.loginfo('param name: %s, using default: %s',
                          name,
                          default_param)
            param = default_param
        else:
            rospy.loginfo('param name: %s, using rosparam: %s',
                          name,
                          ros_param)
            param = ros_param
        setattr(node, name, param)

    ok = True
    for paramname in ('camera_params_module',
                      'pipeline_module',
                      'net_module',
                      'weights_fname'):
        if not getattr(node, paramname):
            rospy.logwarn('Error: no %s specified', paramname)
            ok = False

    rospy.loginfo('subscribing to %s', node.image_topic)
    node.sub_img = rospy.Subscriber(node.image_topic,
                                    Image,
                                    node.classify)

    if node.enable_image_pp:
        rospy.loginfo('publishing image_pp to %s',
                      node.image_pp_topic)
        node.pub_img_pp = rospy.Publisher(node.image_pp_topic,
                                          Image,
                                          queue_size=1)

    if node.enable_labels_viz_blend:
        rospy.loginfo('publishing labels_viz_blend to %s',
                      node.labels_viz_blend_topic)
        node.pub_labels_viz_blend = rospy.Publisher(node.labels_viz_blend_topic,
                                                    Image,
                                                    queue_size=1)

    if node.enable_labels_viz:
        rospy.loginfo('publishing labels_viz to %s',
                      node.labels_viz_topic)
        node.pub_labels_viz = rospy.Publisher(node.labels_viz_topic,
                                              Image,
                                              queue_size=1)

    if node.enable_labels:
        rospy.loginfo('publishing labels tensors to %s', node.labels_topic)
        node.pub_labels = rospy.Publisher(node.labels_topic,
                                          LabelsTensor,
                                          queue_size=1)

    if node.enable_argmax_labels:
        rospy.loginfo('publishing argmax labels tensors to %s', node.argmax_labels_topic)
        node.pub_argmax_labels = rospy.Publisher(node.argmax_labels_topic,
                                                 Image,
                                                 queue_size=1)

    if node.enable_labels_caminfo:
        rospy.loginfo('publishing labels caminfo to %s', node.labels_caminfo_topic)
        node.pub_labels_caminfo = rospy.Publisher(node.labels_caminfo_topic,
                                                  CameraInfo,
                                                  queue_size=1)
    return ok


def init_palette(palette_fname):
    with open(palette_fname, 'r') as f:
        tmp_palette = json.load(f)
        colors = OrderedDict()
        for k, v in tmp_palette.items():
            # by convention 0 is invalid
            if int(k) == 0:
                continue
            colors[int(k)-1] = v
        palette = np.zeros((np.max(colors.keys())+1, 3),
                           dtype='uint8')
        for lbl_src, lbl_dst in colors.items():
            palette[lbl_src] = lbl_dst
    return palette


def init_class_mapping(class_mapping_fname):
    with open(class_mapping_fname, 'r') as f:
        mapping = {int(k): v for k, v in json.load(f).items()}
        label_tab = np.empty((np.max(mapping.keys())+1),
                             dtype='int64')
        label_tab.fill(0)
        for lbl_src, lbl_dst in mapping.items():
            label_tab[lbl_src] = lbl_dst

    # for i in range(len(self.palette)):
        # print i, self.palette[i]
    return label_tab


def _make_labels_caminfo_msg(node, header):
    camera_info = CameraInfo()
    camera_info.header = copy.deepcopy(header)
    camera_info.width = node.camera_params.y_dims[1]
    camera_info.height = node.camera_params.y_dims[0]
    camera_info.K = [node.camera_params.l_fx, 0., node.camera_params.l_cx,
                     0., node.camera_params.l_fy, node.camera_params.l_cy,
                     0., 0., 1.]
    camera_info.P = [node.camera_params.l_fx, 0., node.camera_params.l_cx, 0.,
                     0., node.camera_params.l_fy, node.camera_params.l_cy, 0.,
                     0., 0., 1., 0.]
    camera_info.D = [0., 0., 0., 0., 0.]
    return camera_info


def make_labels_msg(node, header, yhat):
    """ tensor is shaped as num_classes x height x width,
    where height==rows and columns==width.
    """
    # assert((yhat >= 0.).all() and (yhat <= 1.0).all())
    tmsg = LabelsTensor()
    tmsg.header = copy.deepcopy(header)
    tmsg.compression = 'none'
    # TODO get this from experiment file
    if node.num_classes == 1:
        classes = ['valid', 'car']
    else:
        classes = ["building",
                   "vehicle",
                   "ground_pavement",
                   "ground_dirt",
                   "ground_grass",
                   "high_vegetation",
                   "low_vegetation",
                   "manmade",
                   "object",
                   "person",
                   "hills",
                   "sky",
                   "water"]

    #  all pixels valid - let's use 255 by convention
    yhat = (yhat*255).clip(0, 255).astype('u1')
    valid = np.ones((1,)+yhat.shape[1:], dtype=yhat.dtype)*255
    yhat = np.concatenate([valid, yhat], 0)

    tmsg.classes = classes
    tmsg.camera_info = _make_labels_caminfo_msg(node, header)
    tmsg.labels = ndma.ndarray_to_multiarray(yhat, labels=['class', 'rows', 'columns'])

    return tmsg


def make_argmax_labels_msg(node, header, yhat):
    if (yhat.shape[0] == 1):
        yhat_amax = (yhat > 0.5).squeeze().astype('u1')
    else:
        yhat_amax = np.argmax(yhat, 0).astype('u1')
    # rospy.loginfo("yhat_amax: %d", yhat_amax.max())
    msg = bridge.cv2_to_imgmsg(yhat_amax)
    # rospy.loginfo("yhat_amax enc: %s", msg.encoding)
    msg.header = copy.deepcopy(header)
    return msg


def publish_img_pp(node, blob, msg):
    if node.enable_image_pp:
        msgpp = blob['rgb_image_pp_msg']
        msgpp.header = msg.header
        # TODO hack because rqt doesn't undertand 8UC3 now?
        msgpp.encoding = 'bgr8'
        node.pub_img_pp.publish(msgpp)


def publish_labels(node, tmsg):
    if node.enable_labels:
        node.pub_labels.publish(tmsg)


def publish_argmax_labels(node, amax_msg):
    if node.enable_argmax_labels:
        node.pub_argmax_labels.publish(amax_msg)


def publish_labels_caminfo(node, header):
    if node.enable_labels_caminfo:
        cimsg = _make_labels_caminfo_msg(node, header)
        node.pub_labels_caminfo.publish(cimsg)


def publish_labels_viz(node, header, imgpp, norm_yhat):
    if node.enable_labels_viz:
        assert((norm_yhat >= 0.).all() and (norm_yhat <= 1.0).all())
        norm_yhat = (norm_yhat*255).clip(0, 255).astype('u1')
        if node.num_classes == 1:
            _publish_labels_viz_mono(node, header, imgpp, norm_yhat)
        else:
            _publish_labels_viz_multi(node, header, imgpp, norm_yhat)


def _publish_labels_viz_mono(node, header, imgpp, yhat):
    """ publish image with label confidence for cars,
    0-255. Image is resized to square crop on center.
    """
    if yhat.ndim == 3 and yhat.shape[0] == 1:
        yhat = yhat.squeeze()
    # yhat_viz = pl.cm.jet(yhat[0], bytes=True)[...,:3]
    yhat_viz = cv2.resize(yhat, node.camera_params.x_dims, None, 0, 0, cv2.INTER_NEAREST)
    yhat_viz_msg = bridge.cv2_to_imgmsg(yhat_viz, encoding='mono8')
    yhat_viz_msg.header = header
    # print yhat.dtype, yhat.shape, yhat_viz_msg.encoding
    node.pub_labels_viz.publish(yhat_viz_msg)

    zeros = np.zeros_like(yhat_viz)
    yhat_viz3 = np.dstack([yhat_viz, zeros, zeros])
    yhat_blend = cv2.addWeighted(yhat_viz3, 0.5, imgpp[:, :, ::-1], 0.8, 0.)
    yhat_blend_msg = bridge.cv2_to_imgmsg(yhat_blend, encoding='rgb8')
    node.pub_labels_viz_blend.publish(yhat_blend_msg)


def _publish_labels_viz_multi(node, header, imgpp, yhat):
    if not hasattr(node, 'palette'):
        colors = OrderedDict()
        for ix in xrange(node.num_classes+1):
            colors[ix] = distinct_colors_int[ix]
        node.palette = np.empty((np.max(colors.keys())+1, 3), dtype='uint8')
        node.palette[:, :] = [255, 255, 255]
        for lbl_src, lbl_dst in colors.items():
            node.palette[lbl_src] = lbl_dst

    if imgpp.ndim == 2:
        # grayscale images
        imgpp = np.dstack((imgpp, imgpp, imgpp))

    yhat_argmax = np.argmax(yhat, 0)  # network has no notion of 0
    # if hasattr(node, 'class_mapping'):
    #    yhat_argmax = node.class_mapping[yhat_argmax]
    yhat_viz = node.palette[yhat_argmax]

    yhat_viz = cv2.resize(yhat_viz, (imgpp.shape[1], imgpp.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

    yhat_blend = cv2.addWeighted(yhat_viz, 0.5, imgpp[:, :, ::-1], 0.5, 0.)
    # yhat_blend = cv2.addWeighted(yhat_viz, 0.1, imgpp, 0.9, 0.)

    # TODO RGB encoding?
    yhat_blend_msg = bridge.cv2_to_imgmsg(yhat_blend, encoding='rgb8')
    yhat_blend_msg.header = header
    node.pub_labels_viz_blend.publish(yhat_blend_msg)

    yhat_viz_msg = bridge.cv2_to_imgmsg(yhat_viz, encoding='rgb8')
    yhat_viz_msg.header = header
    node.pub_labels_viz.publish(yhat_viz_msg)
