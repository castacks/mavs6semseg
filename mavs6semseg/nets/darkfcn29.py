import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from fosforo.models import FosforoModel
from fosforo.utils.torch_utils import LayerFactory
from fosforo.layers.misc import center_crop_2d
from fosforo.init import bilinear_init_


M = 64
conv = LayerFactory(nn.Conv2d, kernel_size=3, stride=1, padding=1)
pool = LayerFactory(nn.MaxPool2d, kernel_size=2, stride=2)
deconv = LayerFactory(nn.Upsample, mode='bilinear', align_corners=True)
nin = LayerFactory(nn.Conv2d, kernel_size=1, stride=1, padding=0)
spdrop = LayerFactory(nn.Dropout2d, p=0.1)


class Subnet1(FosforoModel):
    def __init__(self):
        super(Subnet1, self).__init__()
        # when adding padding output of pool04/up1_out is 34x34
        self.conv01 = nn.utils.weight_norm(conv(3, 32, padding=49))
        # with 32 padding only 1 zero out in crop04
        #self.conv01 = conv(3, 32, padding=32)
        # with 16 this makes output 29, 29, no zeros
        #self.conv01 = conv(3, 32, padding=16)

        # note switching relu-pool order here
        # - it's pretty much the same but faster
        self.pool01 = pool()
        self.relu01 = nn.LeakyReLU()
        # 448 -> 224 (/2)

        self.conv02 = nn.utils.weight_norm(conv(32, 64))
        self.pool02 = pool()
        self.relu02 = nn.LeakyReLU()
        # 448 -> 112 (/4)

        self.conv03 = nn.utils.weight_norm(conv(64, 128))
        self.relu03 = nn.LeakyReLU()
        self.conv04 = nn.utils.weight_norm(nin(128, 64))
        self.relu04 = nn.LeakyReLU()
        self.conv05 = nn.utils.weight_norm(conv(64, 128))
        self.pool03 = pool()
        self.relu05 = nn.LeakyReLU()
        # 448 -> 56 (/8)

        self.conv06 = nn.utils.weight_norm(conv(128, 256))
        self.relu06 = nn.LeakyReLU()
        self.spdrp6 = spdrop()
        self.conv07 = nn.utils.weight_norm(nin(256, 128))
        self.relu07 = nn.LeakyReLU()
        self.conv08 = nn.utils.weight_norm(conv(128, 256))
        self.relu08 = nn.LeakyReLU()
        self.pool04 = pool()
        # 448 -> 28 (/16)

        self.conv09 = nn.utils.weight_norm(conv(256, 512))
        self.relu09 = nn.LeakyReLU()
        self.spdrp9 = spdrop()
        self.conv12 = nn.utils.weight_norm(nin(512, 256))
        self.relu12 = nn.LeakyReLU()
        self.conv13 = nn.utils.weight_norm(conv(256, 512))
        self.relu13 = nn.LeakyReLU()
        self.pool05 = pool()
        # 448 -> 14 (/32)

        self.conv14 = nn.utils.weight_norm(conv(512, 1024))
        self.relu14 = nn.LeakyReLU()
        self.spdrp14 = spdrop()
        self.conv17 = nn.utils.weight_norm(nin(1024, 512))
        self.relu17 = nn.LeakyReLU()
        self.conv18nin = nn.utils.weight_norm(nin(512, M, bias=None))
        self.relu18nin = nn.LeakyReLU()


class Net(FosforoModel):
    def __init__(self,
                 num_classes,
                 prob_out,
                 crop_hw):
        super(FosforoModel, self).__init__()
        self.num_classes = num_classes
        self.prob_out = prob_out
        # TODO should crop be calculated dynamically?
        self.crop_hw = crop_hw

        self.net1 = Subnet1()

        self.pool04nins = nn.utils.weight_norm(conv(256, M, bias=None))
        self.pool05nins = nn.utils.weight_norm(conv(512, M, bias=None))

        self.relu18ninup = deconv(scale_factor=2)
        self.pool05up = deconv(scale_factor=2)

        self.cls_nin = conv(M, self.num_classes)

        self._initialize_weights()

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight.data, a=0.1)
                if m.bias is not None:
                    m.bias.data.zero_()
            #nn.init.kaiming_normal(self.cls_nin.weight, a=1.0)
            if isinstance(m, nn.ConvTranspose2d):
                bilinear_init_(m.weight)
        nn.init.xavier_normal_(self.cls_nin.weight, gain=0.001)

    def forward(self, x):
        ret = self.net1.seq_forward_fetch(x, ('relu18nin', 'pool04', 'pool05'))

        pool04nins_out = F.leaky_relu(self.pool04nins(ret['pool04']))
        pool05nins_out = F.leaky_relu(self.pool05nins(ret['pool05']))

        outh, outw = self.crop_hw
        pool04nins_crop_out = center_crop_2d(pool04nins_out, outh, outw)
        pool05ninsup_crop_out = center_crop_2d(self.pool05up(pool05nins_out), outh, outw)
        relu18ninup_crop_out = center_crop_2d(self.relu18ninup(ret['relu18nin']), outh, outw)

        fuse_out = pool04nins_crop_out + pool05ninsup_crop_out + relu18ninup_crop_out
        cls_nin_out = self.cls_nin.forward(fuse_out)


        if self.prob_out:
            if self.num_classes==1:
                return torch.sigmoid(cls_nin_out)
            else:
                return torch.softmax(cls_nin_out, dim=1)
        else:
            return cls_nin_out
