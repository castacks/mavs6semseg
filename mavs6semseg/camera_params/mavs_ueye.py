"""
some magic numbers related to images sizes for the camera
and the net.
TODO make this more elegant, maybe use ros param.
for now hard coded.

In this version some downscaling is applied to the image.
"""

width = 1280
height = 1024
fx = 1072.844
fy = 1077.032
cx = 640.379
cy = 526.802

# width, height of input (x) and output (y) of net.
x_dims = 896, 896
# 448 with stride 16
y_dims = x_dims[0]/16, x_dims[1]/16

# label images have different intrinsics due to scaling and cropping.
# here we hardcode the relevant calculations.

# focal length (fx, fy) are only scaled to adjust for downscaling from
# 896x896 (cropped from center of image) to 56x56 for a factor of 0.0625.
# the camera center (cx, cy) should also be scaled (same factor
# as focal length) but also shifted because of the initial
# cropping from 1600x1200 to 896x896.
# we assume scaling is isotropic.

ds = float(y_dims[1])/float(height)
l_cx = ds*(cx - (width-height)/2.0)
l_cy = ds*cy
l_fx = ds*fx
l_fy = ds*fy

# fx, fy, cx, cy
# 58.671, 58.90, 28.02, 28.81

pre_scale_shape = (height, width)
crop_to_shape = (x_dims[1], x_dims[0])
post_scale_shape = (x_dims[1], x_dims[0])
