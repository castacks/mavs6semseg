"""
some magic numbers related to images sizes for the camera
and the net.
TODO make this more elegant, maybe use ros param.
for now hard coded.

In this version cropping is used instead of downscaling.
"""

width = 1600
height = 1200
fx = 1291.145994
fy = 1290.979566
cx = 776.259600
cy = 590.71830

# label images have different intrinsics due to scaling and cropping.
# here we hardcode the relevant calculations.

# focal length (fx, fy) are only scaled to adjust for downscaling from
# 896x896 (cropped from center of image) to 56x56 for a factor of 0.0625.
# the camera center (cx, cy) should also be scaled (same factor
# as focal length) but also shifted because of the initial
# cropping from 1600x1200 to 896x896.
# we assume scaling is isotropic.

x_dims = (896, 896)
y_dims = (56, 56)

ds = float(y_dims[1])/float(x_dims[1])
l_cx = ds*(cx - (width-x_dims[1])/2.0)
l_cy = ds*(cy - (height-x_dims[0])/2.0)
l_fx = ds*fx
l_fy = ds*fy

pre_scale_shape = (height, width)
crop_to_shape = (x_dims[1], x_dims[0])
post_scale_shape = (x_dims[1], x_dims[0])
