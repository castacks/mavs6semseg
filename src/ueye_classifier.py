#!/usr/bin/env python

import importlib
import imp

from spath import Path

import traitlets as tr
import torch
from torch.autograd import Variable

import rospy

from fosforo import checkpoints
# from fideo import dump_info

from fosforo.utils.timing_utils import OneShotTimer

# note utils actually implements a lot of the Node functionality
from mavs6semseg import utils


class Node(tr.HasTraits):
    """ Node object.
    Two use cases:
    - running live on the robot
    - running offline to classify a bag, and save messages in bag

    For the second case we do not necessarily want/need node init and rospy
    param.
    """

    # fixed
    image_topic = 'image'
    image_pp_topic = 'image_pp'
    labels_topic = 'labels'
    mono_labels_topic = 'mono_labels'
    labels_viz_blend_topic = 'labels_viz_blend'
    labels_viz_topic = 'labels_viz'
    argmax_labels_topic = 'argmax_labels'
    labels_caminfo_topic = 'labels_camera_info'

    # default
    node_name = tr.Unicode('classifier_node')

    # required
    net_module = tr.Unicode()
    weights_fname = tr.Unicode()
    num_classes = tr.Int()
    camera_params_module = tr.Unicode()
    pipeline_module = tr.Unicode()

    # optional
    enable_image_pp = tr.Bool(True)
    enable_labels = tr.Bool(True)
    enable_labels_viz_blend = tr.Bool(True)
    enable_labels_viz = tr.Bool(True)
    enable_argmax_labels = tr.Bool(True)
    enable_labels_caminfo = tr.Bool(True)


    palette_fname = tr.Unicode()
    class_mapping_fname = tr.Unicode()
    do_flip = tr.Bool(True)
    skip_every_nth = tr.Int(0)


    def __init__(self, use_ros=True):
        super(Node, self).__init__()
        self.use_ros = use_ros
        self._init_done = False
        self._init_torch_done = False
        if self.use_ros:
            rospy.init_node(self.node_name)
            ok = utils.init_traits_rosparam(self)
            if not ok:
                raise RuntimeError('Error in parameters')
        self._init_net()
        self.skip_ctr = 0

    def _init_net(self):
        self.camera_params = importlib.import_module(self.camera_params_module)
        self._init_pipeline()

        if self.palette_fname:
            self.palette = utils.init_palette(self.palette_fname)

        if self.class_mapping_fname:
            self.class_mapping = utils.init_class_mapping(self.class_mapping_fname)
        self._init_done = True

    def _init_pipeline(self):
        rospy.loginfo('init pipeline')
        pipeline = importlib.import_module(self.pipeline_module)
        publish_img_pp = len(self.image_pp_topic) > 0
        self.ops = pipeline.build_test_ops(self.camera_params, publish_img_pp)
        # self.ops.add_callback(ProfileOpCallback())

    def _init_torch(self):
        rospy.loginfo('torch init start')
        net_module = importlib.import_module(self.net_module)
        self.model = net_module.Net(self.num_classes,
                                    prob_out=True,
                                    crop_hw=(self.camera_params.y_dims[1],
                                             self.camera_params.y_dims[0]))
        weights_fname = Path(self.weights_fname)
        rospy.loginfo('loading weights from {}'.format(weights_fname))
        cpt = checkpoints.CheckpointDir.load_fname(weights_fname)
        self.model.load_state_dict(cpt['weights'])
        self.model.cuda()
        self.model.eval()
        self._init_torch_done = True
        rospy.loginfo('torch init done')

    def _preprocess(self, msg):
        """ apply preprocessing pipeline.
        """
        blob = {'rgb_msg': msg, 'stamp': msg.header.stamp}
        self.ops.apply(blob)
        return blob

    def classify(self, msg):
        """ where the work happens.
        """

        if not self._init_done:
            rospy.loginfo('init not done, ignoring message')
            return

        if (msg.width != self.camera_params.width or
                msg.height != self.camera_params.height):
            raise RuntimeError('image size mismatch with camera_params')

        if not self._init_torch_done:
            self._init_torch()


        if self.skip_every_nth > 0:
            if self.skip_ctr % self.skip_every_nth == 0:
                self.skip_ctr += 1
                return
            else:
                self.skip_ctr += 1

        cb_timer = OneShotTimer().tic()

        with OneShotTimer() as pp_timer:
            blob = self._preprocess(msg)

        with OneShotTimer() as cl_timer:
            if self.do_flip:
                img = blob['cnn_image'][None, ...]
                imgr = img[:, :, :, ::-1].copy()
                with torch.no_grad():
                    timg = Variable(torch.from_numpy(img), requires_grad=False).cuda()
                    timgr = Variable(torch.from_numpy(imgr), requires_grad=False).cuda()
                    yhat = self.model.forward(timg).squeeze(0)
                    yhatr = self.model.forward(timgr).squeeze(0)
                norm_yhat = (yhat.cpu().data.numpy() +
                             yhatr.cpu().data.numpy()[:, :, ::-1])/2.
                # norm_yhat = np.max([yhat1, yhat2], axis=0)
            else:
                img = blob['cnn_image'][None, ...]
                with torch.no_grad():
                    timg = Variable(torch.from_numpy(img), requires_grad=False).cuda()
                    yhat = self.model.forward(timg).squeeze(0)
                norm_yhat = yhat.cpu().data.numpy()

        if hasattr(self, 'class_mapping'):
            for k, v in self.class_mapping.items():
                if k != v:
                    # TODO this may not work reliably for multiple mappings
                    norm_yhat[v-1] = (norm_yhat[k-1] + norm_yhat[v-1]).clip(0., 1.)
                    norm_yhat[k-1] = 0.

        utils.publish_img_pp(self, blob, msg)

        tmsg = utils.make_labels_msg(self, msg.header, norm_yhat)
        amax_msg = utils.make_argmax_labels_msg(self, msg.header, norm_yhat)

        utils.publish_argmax_labels(self, amax_msg)

        utils.publish_labels_caminfo(self, msg.header)

        utils.publish_labels(self, tmsg)

        utils.publish_labels_viz(self, msg.header, blob['rgb_image_pp'], norm_yhat)

        cb_timer.toc()
        rospy.loginfo('timing| total: {:.2f} ms, preproc: {:.2f}, net: {:.2f} ms'
                      .format(cb_timer.elapsed*1000,
                              pp_timer.elapsed*1000,
                              cl_timer.elapsed*1000))
        return tmsg if self.use_ros else None

    def rosloop(self):
        rate = rospy.Rate(1.)  # hz
        while not rospy.is_shutdown():
            rospy.loginfo('%s hearbeat', self.node_name)
            rate.sleep()


if __name__ == "__main__":
    try:
        node = Node(use_ros=True)
        node.rosloop()
    except rospy.ROSInterruptException:
        pass
